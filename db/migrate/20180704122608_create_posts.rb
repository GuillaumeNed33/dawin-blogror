class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.string :title
      t.datetime :postDate
      t.text :header
      t.string :thumb

      t.timestamps
    end
  end
end
