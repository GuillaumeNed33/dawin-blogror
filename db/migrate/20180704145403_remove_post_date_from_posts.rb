class RemovePostDateFromPosts < ActiveRecord::Migration[5.2]
  def change
    remove_column :posts, :postDate, :datetime
  end
end
