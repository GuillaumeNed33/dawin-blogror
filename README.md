# README

Specify your ruby version in Gemfile

add your `config/database.yml` file 

install gems with the command :  

`bundle install`

Set the database configuration :

`rake db:migrate`

Start the server : 

`rails s`

