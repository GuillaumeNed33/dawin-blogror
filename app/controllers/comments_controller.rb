class CommentsController < ApplicationController
  before_action :authenticate_user!

  def create
    authorized_attributes = params.require(:comment).permit(
        :body
    )
    @comment = Comment.new authorized_attributes
    @comment.user = current_user
    @comment.update_attributes params[:comment].permit(get_comment_params)

    if @comment.save
      redirect_to(post_path(@comment.post))
    else
      redirect_to(post_path(@comment.post))
    end
  end

  def get_comment_params
    return :body, :post_id
  end

  def destroy
    @comment = Comment.find(params[:id])
    if @comment.user == current_user #check rigths of current user
      post = @comment.post
      @comment.destroy
      redirect_to post_path(post)
    else
      render 'post/error'
    end
  end

end
