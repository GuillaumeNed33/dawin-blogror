class PostsController < ApplicationController
  before_action :load_post, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:new, :edit, :update, :destroy, :create]
  def index
    @posts = Post.all.paginate(:page => params[:page]).order(updated_at: :desc)
  end

  def load_post
    @post = Post.find(params[:id])
    @comment = Comment.new
  end

  def new
    @post = Post.new
  end

  def create
    authorized_attributes = params.require(:post).permit(
        :title,
        :header,
        :body,
        :thumb
    )
    @post = Post.new authorized_attributes
    @post.user = current_user

    if @post.save
      redirect_to(posts_path)
    else
      render 'new'
    end
  end

  def edit
    if @post.user == current_user #check rigths of current user
      render 'edit'
    else
      render 'posts/error'
    end
  end

  def update
    if @post.user == current_user #check rigths of current user
      authorized_attributes = params.require(:post).permit(
          :title,
          :header,
          :body,
          :thumb
      )
      if @post.update_attributes authorized_attributes
        redirect_to(post_path)
      else
        render 'edit'
      end
    else
      render 'post/error'
    end
  end

  def destroy
    if @post.user == current_user #check rigths of current user
      @post.destroy
      redirect_to posts_path
    else
      render 'post/error'
    end
  end
end
