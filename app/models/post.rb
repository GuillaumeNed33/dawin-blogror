class Post < ApplicationRecord
  belongs_to :user
  validates :title, :header, :body , presence: true
  has_many :comments, dependent: :destroy
  mount_uploader :thumb, ThumbUploader
  self.per_page = 10
end
